<?php
echo "<pre>";
//******** CONSTANTES **********
define('PAGINAS', 10);
echo 'O valor da minha constante é:' . PAGINAS;

$ip_do_banco = '192.168.18.5';
define('IP_DO_BANCO', $ip_do_banco);
echo "\nO IP do SGBD é: " . IP_DO_BANCO;

//Constante mágica
echo "\nEstou na linha:" . __LINE__; //Me da o número da linha
echo "\nArquivo: " . __FILE__; //Me dá o nome do arquivo

//Depurar código
echo "\n";
var_dump($ip_do_banco);

//************* VETORES *************
//Declarações
$dias_da_semana = ['Dom','Seg','Ter','Qua','Qui','Sex', 'Sab'];
var_dump($dias_da_semana);

//Destruir variavel
unset($dias_da_semana);

echo "</pre>";