<?php

//Criar vetor com mais de uma dimensão
//Biruliro é uma referencia da aula de Java script :D
echo '<pre>';
$biruliro[0]['Nome'] = "Ciliro";
$biruliro[0]['Bitbucket'] = "bitbucket.com/ciliro";
$biruliro[1]['Nome'] = "Josephino";
$biruliro[1]['Bitbucket'] = "bitbucket.com/jojo";
$biruliro[2]['Nome'] = "Waltin";
$biruliro[2]['Bitbucket'] = "bitbucket.com/tft";
$biruliro[3]['Nome'] = "Monkey D. Luffy";
$biruliro[3]['Bitbucket'] = "bitbucket.com/neji";
$biruliro[4]['Nome'] = "Roronoa Zoro";
$biruliro[4]['Bitbucket'] = "bitbucket.com/Zzzzz";

$i = 0;
echo "<table>";
$cor = 'white';
//Mudando a cor com IF e Else
while($i < 5){
	if($cor == 'white'){
		$cor = 'blue';
}else{
	$cor='white';
}
	echo "<tr bgcolor='$cor'>";
	//Linha de nomes
	echo "<td style='border: 1px solid black'>";
	echo $biruliro[$i]['Nome'];
	echo "</td>";
	//Linha de zeldas
	echo "<td style='border: 1px solid black'>";
	echo $biruliro[$i]['Bitbucket'];
	echo "</td>";
	echo "</tr>";

	$i++;
}
echo "</table>";

echo "<hr>";

//Mesma tabela só que usando IF 
/*echo "<table>";
$cor = 'blue';
while($i < 5){
	$cor = $cor == 'blue' ? 'white' : 'blue';
	echo "<tr bgcolor='$cor'>";
	//Linha de nomes
	echo "<td style='border: 1px solid black'>";
	echo $biruliro[$i]['Nome'];
	echo "</td>";
	//Linha de zeldas
	echo "<td style='border: 1px solid black'>";
	echo $biruliro[$i]['Bitbucket'];
	echo "</td>";
	echo "</tr>";

	$i++;
}
echo "</table>";

echo "<hr>";*/

//Operadores === e !==
$nome = "Beatriz Gomes da Silva";
$pos = strpos($nome, 'Beatriz');
if($pos !== false){
	echo "Encontrei na posição $pos!";
}